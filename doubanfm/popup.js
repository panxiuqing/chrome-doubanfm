/**
 * Created with JetBrains WebStorm.
 * User: jackpan
 * Date: 12-12-23
 * Time: 下午6:11
 * To change this template use File | Settings | File Templates.
 */
var url = "http://douban.fm/j/mine/playlist";
var data = {type:'n', channel:1};
var playlist = [];
var labels = {};
var song = {};

function get_playlist(tp) {
    data.type = tp;
    if (song.sid != null) {
        data.sid = song.sid;
    }

    $.get(url, data, function(res) {
        playlist = res.song;
        if (tp == 's' || tp == 'n') {
            play_song();
        }
    });
}

function play_song() {
    if (data.channel == -3) {
        heart_icon(ctx1, 0, 0, 50, 'RED', true);
        labels.audio.toggleClass('remarked');
    }
    else {
        heart_icon(ctx1, 0, 0, 50, 'GRAY', true);
        labels.audio.removeClass('remarked');
    }
    song = playlist.pop();
    labels.img.attr("src", song.picture.replace(/mpic/, "lpic"));
    labels.audio.attr("src", song.url);
    labels.artist.text(song.artist);
    labels.album.text('<' + song.albumtitle + '> ' + song.public_time);
    labels.title.text(song.title);
    labels.audio[0].load();
    labels.audio[0].play();
}

function next_song() {
    get_playlist('s');
}

//Draw Icons
function pause_icon(ctx, x, y, size) {
    ctx.clearRect(x, y, size, size);
    ctx.fillStyle = 'WHITE';
    ctx.fillRect(size/5, size/5, size/5, size*3/5);
    ctx.fillRect(size*3/5, size/5, size/5, size*3/5);
}

function play_icon(ctx, x, y, size) {
    ctx.clearRect(x, y, size, size);
    ctx.fillStyle = 'WHITE';
    ctx.beginPath();
    ctx.moveTo(size/5, size/5);
    ctx.lineTo(size*4/5, size/2);
    ctx.lineTo(size/5, size*4/5);
    ctx.fill();
}

function heart_icon(ctx, x, y, size, color, fill) {
    ctx.clearRect(x, y, size, size);
    var x1 = x + size * 2 / 3;
    var y1 = y + size / 3;
    var r1 = size / 6 * Math.sqrt(2);
    var x2 = x + size / 2;
    var y2 = y1;
    var r2 = size / 6 * Math.sqrt(5);
    var x3 = x + size / 6;
    var y3 = y + size / 6;
    var r3 = size / 3 * Math.sqrt(5);
    var pi = Math.PI;
    var ata = Math.atan(1/2);

    ctx.beginPath();
    ctx.arc(x1, y1, r1, 225*pi/180, 315*pi/180);
    ctx.arc(x2, y2, r2, 2*pi-ata, ata);
    ctx.arc(x3, y3, r3, ata, pi/2 - ata);
    ctx.arc(x3 + size*2/3, y3, r3, pi/2 + ata, pi-ata);
    ctx.arc(x2, y2, r2, pi-ata, pi+ata);
    ctx.arc(x1 - size/3, y1, r1, 225*pi/180, 315*pi/180);
    if (fill) {
        ctx.fillStyle = color;
        ctx.fill();
    }
    else {
        ctx.strokeStyle = color;
        ctx.stroke();
    }
}

function trash_icon(ctx, x, y, size, color) {
    ctx.clearRect(x, y, size, size);
    xl = x + size / 6;
    xr = x + size / 6 * 5;
    y1 = y + size / 6;
    y2 = y + size / 4;
    y3 = y + size / 24 *7;
    y4 = y + size / 24 * 19;
    r = size / 24;
    pi = Math.PI;

    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.fillRect(xl, y1, size*2/3, size/12);
    ctx.arc(x+size/2, y1, r, pi, 2*pi);
    ctx.arc(xr, y1+r, r, pi*3/2, pi/2);
    ctx.arc(xl, y1+r, r, pi/2, pi*3/2);
    ctx.fill();
    ctx.fillRect(xl, y3, size*2/3, size/2);
    ctx.fillRect(xl+size/24, y4, size*7/12, size/24);

    ctx.moveTo(xl+size/24, y4);
    ctx.arc(xl+size/24, y4, r, pi/2, pi);
    ctx.arc(xr-size/24, y4, r, 0, pi/2);
    ctx.fill();

    ctx.fillStyle = 'WHITE';
    ctx.fillRect(xl+size/6, y3+size/12, size/12, size/3);
    ctx.fillRect(xl+size*5/12, y3+size/12, size/12, size/3);
}

function next_icon(ctx, x, y, size, color) {
    ctx.clearRect(x, y, size, size);
    var x1 = x + size/12;
    var x2 = x + size*5/12;
    var x3 = x + size*3/4;
    var y1 = y + size/4;
    var y2 = y + size/2;
    var y3 = y + size*3/4;
    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.lineTo(x1, y3);
    ctx.fill();
    ctx.moveTo(x2, y1);
    ctx.lineTo(x3, y2);
    ctx.lineTo(x2, y3);
    ctx.fill();
    ctx.fillRect(x3, y1, size/6, size/2);
}

var ctx0;
var ctx1;
var ctx2;
var ctx3;

onload = function() {
    var cvs0 = $('#pause');
    ctx0 = cvs0[0].getContext('2d');
    var cvs1 = $('#remark');
    ctx1 = cvs1[0].getContext('2d');
    var cvs2 = $('#bye');
    ctx2 = cvs2[0].getContext('2d');
    var cvs3 = $('#next');
    ctx3 = cvs3[0].getContext('2d');
    heart_icon(ctx1, 0, 0, 50, 'GRAY', true);
    trash_icon(ctx2, 0, 0, 50, 'GRAY');
    next_icon(ctx3, 0, 0, 50, 'GRAY');
    pause_icon(ctx0, 0, 0, 25);


    channels = [{id: 0, name:'我的私人兆赫'},
                {id: -3, name:'我的红心兆赫'},
                {id: 1, name: '华语 MHz'},
                {id: 2, name: '欧美 MHz'},
                {id: 4, name: '八零 MHz'}];
    for(i=0; i<channels.length; i++) {
        $('.channel').append('<li class="chl_name"><a id=' + channels[i].id + '>' + channels[i].name + '</a></li>');
    }

};

$(document).ready(function() {
    //All related elements
    labels = {
        img: $('img'),
        audio: $('audio'),
        artist: $('.artist'),
        album: $('.albumtitle'),
        title: $('.song')
    };
    
    //Add listener to audio elements
    labels.audio.bind('ended', function() {
        play_song();
        if (playlist == '') {
            get_playlist('p'); 
        }
    })

    //Start Play
    get_playlist('n');

    //channel apear change
    $('.chl_name').live('click', function() {
        $('.chl_name_click').toggleClass('chl_name_click');
        $(this).toggleClass('chl_name_click');
        data.channel = $(this).children('a').attr('id');
        get_playlist('n');
    })

    //Click image animation
    $('.img').click(function() {
        $(this).toggleClass('img_click');
    })

    $('#pause').live('click', function() {
        if ($(this).parent().hasClass('btn-pause-click')) {
            pause_icon(ctx0, 0, 0, 25);
            labels.audio[0].play();
        }
        else {
            play_icon(ctx0, 0, 0, 25);
            labels.audio[0].pause();
        }
        $(this).parent().toggleClass('btn-pause-click');
    })
    $('#remark').click(function() {
        var color;
        if ($(this).hasClass('remarked')) {
            data.type = 'u';
            color = 'GRAY';
        }
        else {
            data.type = 'r';
            color = 'RED';
        }
        $(this).toggleClass('remarked');
        data.sid = song.sid;
        $.get(url, data, function(res) {
            heart_icon(ctx1, 0, 0, 50, color, true);
        })
    });
    $('#bye').click(function() {
        data.type = 'b';
        data.sid = song.sid;
        $.get(url, data, function(res) {
          next_song();
        })
    });
    $('#next').click(function() {
        next_song();
    });
});
